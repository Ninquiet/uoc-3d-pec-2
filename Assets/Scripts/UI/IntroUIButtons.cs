using Cinemachine;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

using UnityEngine;

namespace UI
{
    public class IntroUIButtons : MonoBehaviour
    {
        [SerializeField] 
        private CinemachineVirtualCamera _generalMenuCamera;
        [SerializeField] 
        private CinemachineVirtualCamera _tutorialCamera;
        
        [SerializeField]
        private CanvasGroup _generalMenuCanvasGroup;
        [SerializeField]
        private CanvasGroup _tutorialCanvasGroup;
        
        public void ShowTutorial()
        {
            _generalMenuCamera.Priority = 0;
            _tutorialCamera.Priority = 1;
            _generalMenuCanvasGroup.interactable = false;
            _generalMenuCanvasGroup.blocksRaycasts = false;
            _generalMenuCanvasGroup.DOFade(0, 0.3f);
            _tutorialCanvasGroup.interactable = true;
            _tutorialCanvasGroup.blocksRaycasts = true;
            _tutorialCanvasGroup.DOFade(1, 0.3f);
        }
        public void StartGame()
        {
            Debug.Log("Start Game");
            SceneManager.LoadScene(1); 
        }

        public void ExitGame()
        {
            Debug.Log("Exit Game");
            Application.Quit();
        }
    }
}