using System;
using Character.Life;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UnderAttackAlert : MonoBehaviour
    {
        [SerializeField]
        private LifeController _lifeController;
        [SerializeField] 
        private Image _vignette;

        private void Awake()
        {
            _lifeController.OnBeingHit += HasBeenHit;
        }

        private void HasBeenHit(Vector3 obj)
        {
            _vignette.DOKill();
            _vignette.DOFade(0.5f, 0.25f).
                OnComplete(() => _vignette.DOFade(0, 0.2f));
        }
    }
}