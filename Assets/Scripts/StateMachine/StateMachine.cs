using UnityEngine;

namespace NinquietGame.StateMachine
{
    public abstract class StateMachine<TFactory, TCtx, TVars> : MonoBehaviour
        where TFactory : StateFactory<TFactory, TCtx,TVars>
        where TCtx : class
        where TVars : BaseVars
    {
        public abstract TFactory Factory();
        public abstract TVars Vars();
        public BaseState<TFactory,TCtx,TVars> CurrentState { get; private set; }
        
        public abstract BaseState<TFactory,TCtx,TVars> GetDefaultState();

        public void ChangeState(BaseState<TFactory,TCtx,TVars> state)
        {
            CurrentState?.OnExitStates();
            CurrentState = state;
            CurrentState.OnEnter();
        }
        
        private void Update()
        {
            CurrentState?.OnUpdateStates();
        }
        
        protected virtual void Start()
        {
            ChangeState(GetDefaultState());
        }
    }
}