namespace NinquietGame.StateMachine
{
    public abstract class BaseState<TFactory, TCtx, TVar> 
        where TFactory : StateFactory<TFactory, TCtx,TVar>
        where TCtx : class
        where TVar : BaseVars
    {
        protected BaseState<TFactory, TCtx,TVar>  _subState;
        protected BaseState<TFactory, TCtx,TVar>  _parentState;
        
        protected bool _isRootState;
        protected TFactory _factory;
        protected TCtx _ctx;

        protected BaseState(TFactory stateFactory, TCtx stateMachine, bool isRootState)
        {
            _factory = stateFactory;
            _ctx = stateMachine;
            _isRootState = isRootState;
        }

        public abstract void OnEnter();
        protected abstract void OnExit();
        protected abstract void OnUpdate();
        protected abstract void CheckChangeStateConditions();

        protected void SetSubState(BaseState<TFactory, TCtx, TVar>  state)
        {
            _subState?.OnExitStates();
            _subState = state;
            _subState.SetParentState(this);
            _subState.OnEnter();
        }
        
        protected void ChangeState(BaseState<TFactory, TCtx, TVar> state)
        {
            if (_isRootState)
            {
                (_ctx as StateMachine<TFactory,TCtx,TVar>)?.ChangeState(state);
            }
            else
            {
                _parentState.SetSubState(state);
            }
        }
        
        public void SetParentState(BaseState<TFactory, TCtx, TVar>  state)
        {
            _parentState = state;
        }
        
        public virtual void OnExitStates()
        {
            _subState?.OnExitStates();
            OnExit();
        }
        public virtual void OnUpdateStates()
        {
            OnUpdate();
            _subState?.OnUpdateStates();
            CheckChangeStateConditions();
        }

    }
}