namespace NinquietGame.StateMachine
{
    public abstract class StateFactory <TFactory, TCtx,TVar>
        where TFactory: StateFactory<TFactory,TCtx,TVar>
        where TCtx: class
        where TVar: BaseVars
    {
        protected TCtx _ctx;
        
        public StateFactory(TCtx ctx)
        {
            _ctx = ctx;
        }
        
        public abstract BaseState<TFactory,TCtx,TVar> GetInitialState();
    }
}