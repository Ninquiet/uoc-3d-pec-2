using UnityEngine;

namespace Weapons
{
    public enum WeaponType
    {
        Pistola,
        Ametralladora
    }
    public abstract class WeaponBase : MonoBehaviour
    {
        [SerializeField] 
        protected float _damage;
        [SerializeField] 
        protected GameObject decalPrefab;

        public float ShootDelay = 0.5f;
        public float LastShootTime = 0.0f;
        public WeaponType WeaponType = default;
        public int Ammo = 10;
        public abstract void EnableWeapon();
        public abstract void DisableWeapon();
        public abstract void Shoot();
        
        public void AddAmmo(int amount)
        {
            Ammo += amount;
        }
        
        protected void CreateDecal(RaycastHit hit)
        {
            if (decalPrefab == null) 
                return;
            
            Quaternion decalRotation = Quaternion.LookRotation(hit.normal) * Quaternion.Euler(90f, 0f, 0f);

            GameObject decal = Instantiate(decalPrefab, hit.point, decalRotation);
            decal.transform.Translate(Vector3.up * 0.1f); 
            decal.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f); 
    
            Destroy(decal, 4f); 
        }
    }
}