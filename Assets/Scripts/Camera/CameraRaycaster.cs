using UnityEngine;
public class CameraRaycaster : MonoBehaviour
{
    [SerializeField]
    private Camera _camera;

    private static Camera _CameraStaticRef;
    
    private void Awake()
    {
        _CameraStaticRef = _camera;
    }
    
    public static RaycastHit? GetRaycastHitFromCameraCenter(float maxDistance, LayerMask layerMask)
    {
        Ray ray = _CameraStaticRef.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, maxDistance,layerMask))
        {
            return hit;
        }

        return null;
    }
}