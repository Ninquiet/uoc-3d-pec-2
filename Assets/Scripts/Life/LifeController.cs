using System;
using UnityEngine;

namespace Character.Life
{
    public class LifeController : MonoBehaviour
    {
        [SerializeField] private int _lifePoints = 1;
        [SerializeField] private int _maxLifePoints = 2;
        [SerializeField] private int _currentShield = 0;
        [SerializeField] private int _maxShield = 10;
        
        public Action<int> OnLifePointsChanged;
        public Action<Vector3> OnBeingHit;
        public Action OnDeath;
        public Action<int> OnShieldChanged;

        private void Start()
        {
            OnLifePointsChanged?.Invoke(_lifePoints);
            OnShieldChanged?.Invoke(_currentShield);
        }

        public void SetMaxLifePoints(int maxLifePoints)
        {
            _maxLifePoints = maxLifePoints;
        }
        
        public void SetLifePoints(int lifePoints)
        {
            _lifePoints = lifePoints;
            OnLifePointsChanged?.Invoke(lifePoints);
        }
        
        public void TakeDamage(int damage, Vector3 attackDirection)
        {
            if (_currentShield > 0)
            {
                if (damage <= _currentShield)
                {
                    _currentShield -= damage;
                    damage = 0; // All damage absorbed by shield
                }
                else
                {
                    damage -= _currentShield;
                    _currentShield = 0;
                }
                OnShieldChanged?.Invoke(_currentShield);
            }

            if (damage > 0)
            {
                _lifePoints -= damage;
                if (_lifePoints <= 0)
                {
                    _lifePoints = 0;
                    OnDeath?.Invoke();
                }
                OnLifePointsChanged?.Invoke(_lifePoints);
            }

            OnBeingHit?.Invoke(attackDirection);
        }
        
        public void IncreaseShield(int shield)
        {
            _currentShield += shield;
            _currentShield = Mathf.Clamp(_currentShield, 0, _maxShield);
            OnShieldChanged?.Invoke(_currentShield);
        }
        
        public void Heal(int heal)
        {
            _lifePoints = Mathf.Clamp(_lifePoints + heal, 0, _maxLifePoints);
            OnLifePointsChanged?.Invoke(_lifePoints);
        }
    }
}