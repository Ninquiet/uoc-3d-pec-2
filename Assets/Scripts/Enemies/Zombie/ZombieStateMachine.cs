using Character.Life;
using Enemies.Zombie.ZombieStates;
using NinquietGame.StateMachine;
using UnityEngine;
using UnityEngine.AI;
using Utilities;

namespace Enemies.Zombie
{
    public class ZombieStateMachine : StateMachine<ZombieStateFactory, ZombieStateMachine,ZombieVars>
    {
        [SerializeField]
        private NavMeshAgent _navMeshAgent;
        [SerializeField]
        private ZombieVars _zombieVars = new ();
        [SerializeField]
        private TriggerEventWatcher _viewTrigger;
        [SerializeField]
        private LifeController _lifeController;
        [SerializeField]
        private Collider _physicsCollider;

        private ZombieStateFactory _stateFactory;
        private bool _isDead;
        
        public override ZombieVars Vars() => _zombieVars;
        public override ZombieStateFactory Factory() => _stateFactory;


        public override BaseState<ZombieStateFactory, ZombieStateMachine, ZombieVars> GetDefaultState()
        {
            if (Vars().Waypoints.Length > 0)
                return Factory().WayPointState();
            return Factory().GetInitialState();
        }

        public void MoveToTarget()
        {
            if (_navMeshAgent == null || _isDead) return;
            _navMeshAgent.SetDestination(Vars().TargetDirection);
        }
        
        public void RotateTowardsTarget()
        {
            if (Vars().TargetTransform == null)
                return;

            Vector3 targetPosition = Vars().TargetTransform.position;
            targetPosition.y = transform.position.y;

            Vector3 direction = targetPosition - transform.position;
            if (direction == Vector3.zero) return; 
    
            Quaternion rotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 2);
        }
        
        public float GetDistanceToTarget()
        {
            var _target = Vars().TargetTransform;
            if (_target == null) 
                return 9999 ;
            
            var distance = Vector3.Distance(transform.position, _target.position);
            return distance;
        }
        
        public virtual ZombieBaseState GetAttackState()
        {
            return Factory().AttackState();
        }
        
        public void Destroy(float time = 0)
        {
            Destroy(gameObject, time);
        }

        private void OnTargetExitView(Transform targetTransform)
        { 
            Vars().IsSeeingThePlayer = false;
            Vars().TargetDirection = Vector3.zero;
        }

        private void OnTargetEnterOnView(Transform targetTransform)
        {
            if (targetTransform == null) 
                return;
            
            Vars().IsSeeingThePlayer = true;
            Vars().TargetTransform = targetTransform;
        }
        
        private void Awake()
        {
            _stateFactory = new ZombieStateFactory(this);
            _viewTrigger.OnEnter += OnTargetEnterOnView;
            _viewTrigger.OnExit += OnTargetExitView;
            _lifeController.OnBeingHit += WasHitted;
            _lifeController.OnDeath += HasDead;
        }

        private void WasHitted(Vector3 obj)
        {
            if (_isDead) 
                return;
            if (CurrentState is ZombieIdleState or ZombieWayPointState or ZombieCheckPositionState)
            {
                ChangeState(Factory().CheckPositionState());
                Vars().TargetDirection = transform.position - obj;
            }
            Vars().ZombieSounds.PlaySound(Vars().ZombieSounds._hurtSound);
        }

        private void HasDead()
        {
            if (_isDead) 
                return;
            
            _isDead = true;
            _physicsCollider.enabled = false;
            _navMeshAgent.isStopped = true;
            ChangeState(Factory().DeathState());
        }

        private void OnDestroy()
        {
            _viewTrigger.OnEnter -= OnTargetEnterOnView;
            _viewTrigger.OnExit -= OnTargetExitView;
        }
    }
}