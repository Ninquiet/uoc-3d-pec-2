using Character.Life;
using UnityEngine;
using Utilities;

namespace Enemies.Zombie
{
    public class ZombieBodyAttack: EnemyBaseAttack
    {
        [SerializeField] TriggerEventWatcher _triggerEventWatcher;
        
        private  Transform _target;
        private LifeController _targetLifeController;
        private bool _canAttack;
        
        public override void PerformAttack(float damange)
        {
            if (_canAttack)
            {
                var attackDirection = (_target.position - transform.position).normalized;
                _targetLifeController.TakeDamage((int)damange, attackDirection);
            }
        }
        
        private void OnTargetEnter(Transform targetTransform)
        {
            targetTransform.TryGetComponent<LifeController>(out _targetLifeController);
            if (_targetLifeController == null)
            {
                targetTransform.parent.TryGetComponent<LifeController>(out _targetLifeController);
                if (_targetLifeController == null) return;
            }
            
            _target = targetTransform;
            _canAttack = true;
        }
        
        private void OnTargetExit(Transform targetTransfom)
        {
            if (_target != targetTransfom) return;
            
            _target = null;
            _canAttack = false;
        }
        
        private void Awake()
        {
            _triggerEventWatcher.OnEnter += OnTargetEnter;
            _triggerEventWatcher.OnExit += OnTargetExit;
        }

        private void OnDestroy()
        {
            _triggerEventWatcher.OnEnter -= OnTargetEnter;
            _triggerEventWatcher.OnExit -= OnTargetExit;
        }
    }
}