using Enemies.Zombie.ZombieStates;
using NinquietGame.StateMachine;

namespace Enemies.Zombie
{
    public class ZombieStateFactory : StateFactory<ZombieStateFactory, ZombieStateMachine,ZombieVars>
    {
        public ZombieStateFactory(ZombieStateMachine ctx) : base(ctx)
        {
        }
        
        public ZombieBaseState IdleState() => new ZombieIdleState(this,_ctx,true);
        public ZombieBaseState MoveState() => new ZombieMoveState(this,_ctx,true);
        public virtual ZombieBaseState AttackState() => new ZombieBodyAttackState(this,_ctx,true);
        public ZombieBaseState DeathState() => new ZombieDeathState(this,_ctx,true);
        public ZombieBaseState CheckPositionState() => new ZombieCheckPositionState(this,_ctx,true);
        public ZombieBaseState WayPointState() => new ZombieWayPointState(this,_ctx,true);

        public override BaseState<ZombieStateFactory, ZombieStateMachine,ZombieVars>  GetInitialState()
        {
            return IdleState();
        }
    }
}