using NinquietGame.StateMachine;

namespace Enemies.Zombie.ZombieStates
{
    public abstract class ZombieBaseState : BaseState<ZombieStateFactory, ZombieStateMachine,ZombieVars> 
    {
        public ZombieBaseState(ZombieStateFactory zombieStateFactory, ZombieStateMachine ctx, bool isRootState) : base(zombieStateFactory, ctx, isRootState)
        {
        }

        public override void OnEnter()
        {
        }

        protected override void OnExit()
        {
            
        }

        protected override void OnUpdate()
        {
        }

        protected override void CheckChangeStateConditions()
        {
            
        }
    }
}