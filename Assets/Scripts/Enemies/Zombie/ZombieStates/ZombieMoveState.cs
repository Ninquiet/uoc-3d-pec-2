using UnityEngine;

namespace Enemies.Zombie.ZombieStates
{
    public class ZombieMoveState : ZombieBaseState
    {
        float _lastTimeSound = 0;
        public ZombieMoveState(ZombieStateFactory zombieStateFactory, ZombieStateMachine ctx, bool isRootState) : base(zombieStateFactory, ctx, isRootState)
        {
        }

        public override void OnEnter()
        {
            _ctx.Vars().Animator.SetBool("Walking", true);
            _lastTimeSound = Time.time;
            
        }

        protected override void OnExit()
        {
            
        }

        protected override void OnUpdate()
        {
            PlayIdleSound();
            var targetTransform = _ctx.Vars().TargetTransform;
            if (targetTransform != null)
            {
                _ctx.Vars().TargetDirection = targetTransform.position;
                _ctx.MoveToTarget();
            }
        }

        protected override void CheckChangeStateConditions()
        {
            if (!_ctx.Vars().IsSeeingThePlayer)
            {
                ChangeState(_ctx.GetDefaultState());
            }
            
            if (_ctx.GetDistanceToTarget() < _ctx.Vars().AttackRange)
            {
                ChangeState(_ctx.GetAttackState());
            }
        }
        
        private void PlayIdleSound()
        {
            if (Time.time - _lastTimeSound > 3.5f)
            {
                _ctx.Vars().ZombieSounds.PlaySound( _ctx.Vars().ZombieSounds._idleSound);
                _lastTimeSound = Time.time;
            }
        }
    }
}