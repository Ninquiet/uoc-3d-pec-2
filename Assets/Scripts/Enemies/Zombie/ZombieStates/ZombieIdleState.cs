using UnityEngine;

namespace Enemies.Zombie.ZombieStates
{
    public class ZombieIdleState : ZombieBaseState
    {
        float _lastTimeSound = 0;
        public ZombieIdleState(ZombieStateFactory zombieStateFactory, ZombieStateMachine ctx, bool isRootState) : base(zombieStateFactory, ctx, isRootState)
        {
        }

        public override void OnEnter()
        {
            _ctx.Vars().Animator.SetBool("Walking", false);
            _ctx.Vars().Animator.ResetTrigger("Attack");
            _lastTimeSound = Time.time;
        }

        protected override void OnExit()
        {
            
        }

        protected override void OnUpdate()
        {
            PlayIdleSound();
        }

        protected override void CheckChangeStateConditions()
        {
            if (_ctx.Vars().IsSeeingThePlayer)
            {
                ChangeState(_factory.MoveState());
            }
        }
        
        private void PlayIdleSound()
        {
            if (Time.time - _lastTimeSound > 3.5f)
            {
                _ctx.Vars().ZombieSounds.PlaySound( _ctx.Vars().ZombieSounds._idleSound);
                _lastTimeSound = Time.time;
            }
        }
    }
}