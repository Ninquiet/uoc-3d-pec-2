using Enemies.ZombieDistance;

namespace Enemies.Zombie.ZombieStates
{
    public class ZombieDistanceStateFactory : ZombieStateFactory
    {
        public ZombieDistanceStateFactory(ZombieStateMachine ctx) : base(ctx)
        {
        }
        
        public override ZombieBaseState AttackState() => new ZombieDistanceAttackState(this, _ctx, true);
    }
}