using UnityEngine;
using Weapons.Bullets;

namespace Enemies.Zombie.ZombieStates
{
    [System.Serializable]
    public class ZombieDistanceVars : ZombieVars
    {
        [SerializeField] private Transform _shootOrigin;
        public Transform ShootOrigin => _shootOrigin;
        [SerializeField] private BasicBullet _bulletPrefab;
        public BasicBullet BulletPrefab => _bulletPrefab;
    }
}