using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Utilities
{
    public class Notification : MonoBehaviour
    {
        [SerializeField] TMP_Text _text;
        [SerializeField] Image _background;
        
        public void SetText(string text)
        {
            _text.text = text;
        }
        
        public void SetColor(Color color)
        {
            _background.color = color;
        }
    }
}