using System;
using DG.Tweening;
using UnityEngine;

namespace Utilities
{
    public enum NotificationType
    {
        Ammo,
        Health,
        Shield,
        Key,
        Message
    }
    public class NotificationController : MonoBehaviour
    {
       [SerializeField] 
       private Notification _notificationPrefab;
       [SerializeField]
       private Color _ammoColor;
       [SerializeField] 
       private Color _healtColor;
       [SerializeField] 
       private Color _keyColor;
       [SerializeField] 
       private Color _messageColor;
       [SerializeField]
       private Color _shieldColor;
       
       private static Notification _staticNotificationPrefab;
       private static Transform _staticContainerTransform;
       private static Color _staticAmmoColor;
       private static Color _staticHealthColor;
       private static Color _staticKeyColor;
       private static Color _staticMessageColor;
       private static Color _staticShieldColor;

       private void Awake()
       {
           _staticNotificationPrefab = _notificationPrefab;
           _staticContainerTransform = transform;
           _staticAmmoColor = _ammoColor;
           _staticKeyColor = _keyColor;
           _staticHealthColor = _healtColor;
           _staticMessageColor = _messageColor;
       }
       
       public static void ShowNotification(string message, NotificationType type)
       {
           Notification notification = Instantiate(_staticNotificationPrefab, _staticContainerTransform);
           notification.SetText(message);
           notification.SetColor(GetColorByType(type));
           
           DoNotificationAnimation(notification.GetComponent<RectTransform>());
       }

       private static void DoNotificationAnimation(RectTransform targetRectTransform)
       {
           Canvas canvas = targetRectTransform.GetComponentInParent<Canvas>();
           RectTransform canvasRect = canvas.GetComponent<RectTransform>();

           Vector2 endValue = targetRectTransform.anchoredPosition;
           Vector2 startValue = new Vector2(targetRectTransform.anchoredPosition.x, -canvasRect.rect.height / 2 - targetRectTransform.rect.height);

           targetRectTransform.anchoredPosition = startValue;
           targetRectTransform.DOAnchorPos(endValue, 1f).SetEase(Ease.OutBack)
               .OnComplete(() =>
               {
                   targetRectTransform.DOAnchorPos(startValue, 1f).SetEase(Ease.InBack).SetDelay(1f).OnComplete(() =>
                   {
                       targetRectTransform.DOKill();
                       Destroy(targetRectTransform.gameObject);
                   });
               });
       }
       
       private static Color GetColorByType(NotificationType type)
       {
           switch (type)
           {
               case NotificationType.Ammo:
                   return _staticAmmoColor;
               case NotificationType.Health:
                   return _staticHealthColor;
               case NotificationType.Key:
                   return _staticKeyColor;
               case NotificationType.Message:
                   return _staticMessageColor;
                case NotificationType.Shield:
                    return _staticShieldColor;
               default:
                   throw new ArgumentOutOfRangeException(nameof(type), type, null);
           }
       }
    }
}