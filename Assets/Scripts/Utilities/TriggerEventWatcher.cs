using System;
using UnityEngine;
using UnityEngine.Events;

namespace Utilities
{
    public class TriggerEventWatcher : MonoBehaviour
    {
        public string targetTag;
        public Action<Transform> OnEnter;
        public Action<Transform> OnExit;
        public UnityEvent OnEnterEvent;
        public UnityEvent OnExitEvent;
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(targetTag))
            {
                OnEnter?.Invoke(other.transform);
                OnEnterEvent?.Invoke();
            }
        }
        
        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag(targetTag))
            {
                OnExit?.Invoke(other.transform);
                OnExitEvent?.Invoke();
            }
        }
    }
}