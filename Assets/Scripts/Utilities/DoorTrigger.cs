using System;
using DG.Tweening;
using Player;
using UnityEngine;

namespace Utilities
{
    public class DoorTrigger : MonoBehaviour
    {
        [SerializeField] 
        Transform _leftDoor;
        [SerializeField]
        Transform _rightDoor;
        [SerializeField]
        private string _neededKey;

        private Vector3 _originalLeftPosition;
        private Vector3 _originalRightPosition;
        
        private bool _isOpen = false;

        private void Awake()
        {
            _originalLeftPosition = _leftDoor.position;
            _originalRightPosition = _rightDoor.position;
        }

        private void OnTriggerEnter(Collider other)
        {
            other.TryGetComponent(out PlayerKeysHandler otherKeysHandler);
            if (otherKeysHandler == null)
            {
                    return;
            }
            
            if (otherKeysHandler.HasKey(_neededKey))
                OpenDoor();
            else
                NotificationController.ShowNotification($"Necesitas la llave {_neededKey} para abrir esta puerta.", NotificationType.Message);
        }
        
        private void OpenDoor()
        {
            if (_isOpen)
                return;
           _leftDoor.DOMove( _leftDoor.position + Vector3.right * 1f, 1f);
           _rightDoor.DOMove( _rightDoor.position + Vector3.left * 1f, 1f);
           _isOpen = true;
        }
        
        private void CloseDoor()
        {
            if (!_isOpen)
                return;
            _leftDoor.DOMove(_originalLeftPosition, 1f);
            _rightDoor.DOMove(_originalRightPosition, 1f);
            _isOpen = false;
        }
    }
}