using System;
using Sounds;
using UnityEngine;
using UnityEngine.UI;
using Weapons;

namespace Player
{
    public class PlayerWeaponHandler : MonoBehaviour
    {
        [SerializeField]
        private PlayerSounds _playerSounds;
        [SerializeField] 
        private WeaponBase[] _weapons;
        
        private int _currentWeaponIndex;
        
        public Action<string> OnWeaponChanged;
        public Action<int> OnActiveAmmoChanged;
        
        public void TryShootCurrentWeapon()
        {
            if (_weapons[_currentWeaponIndex].Ammo <= 0)
                return;

            var canShoot = Time.time - _weapons[_currentWeaponIndex].LastShootTime > _weapons[_currentWeaponIndex].ShootDelay;
            if (!canShoot)
                return;
            
            var thereIsNoAmmo = _weapons[_currentWeaponIndex].Ammo <= 0;
            if (thereIsNoAmmo)
                return;
            
            _playerSounds.PlaySound(_playerSounds._ShotSound);
            _weapons[_currentWeaponIndex].Ammo--;
            _weapons[_currentWeaponIndex].LastShootTime = Time.time;
            _weapons[_currentWeaponIndex].Shoot();
            OnActiveAmmoChanged?.Invoke(_weapons[_currentWeaponIndex].Ammo);
        }
        
        public int GetCurrentWeaponMunition()
        {
            return _weapons[_currentWeaponIndex].Ammo;
        }

        public void ChangeWeapon(int weaponIndex)
        {
            if (weaponIndex < 0 || weaponIndex >= _weapons.Length || weaponIndex == _currentWeaponIndex)
            {
                return;
            }
            _weapons[_currentWeaponIndex].DisableWeapon();
            _currentWeaponIndex = weaponIndex;
            _weapons[_currentWeaponIndex].EnableWeapon();
            OnWeaponChanged?.Invoke(_weapons[_currentWeaponIndex].WeaponType.ToString());
            OnActiveAmmoChanged?.Invoke(_weapons[_currentWeaponIndex].Ammo);
        }
        
        public void AddMunition(WeaponType type, int amount)
        {
            GetWeaponByType(type)?.AddAmmo(amount);
            OnActiveAmmoChanged?.Invoke(_weapons[_currentWeaponIndex].Ammo);
        }

        private WeaponBase GetWeaponByType(WeaponType type)
        {
            foreach (var weapon in _weapons)
            {
                if (weapon.WeaponType == type)
                {
                    return weapon;
                }
            }
            
            return null;
        }

        private void Start()
        {
            DissableAllWeapons();
            _currentWeaponIndex = 0;
            _weapons[_currentWeaponIndex].EnableWeapon();
            OnWeaponChanged?.Invoke(_weapons[_currentWeaponIndex].WeaponType.ToString());
            OnActiveAmmoChanged?.Invoke(_weapons[_currentWeaponIndex].Ammo);
        }
        
        private void DissableAllWeapons()
        {
            foreach (var weapon in _weapons)
            {
                weapon.DisableWeapon();
            }
        }
    }
}