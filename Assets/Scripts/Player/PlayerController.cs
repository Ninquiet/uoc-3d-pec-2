using Character.Life;
using Sounds;
using UnityEngine;
using UnityEngine.InputSystem;
using StarterAssets;
using Utilities;

namespace Player
{
    public class PlayerController: MonoBehaviour
    {
        [SerializeField]
        private PlayerSounds _playerSounds;
        [SerializeField]
        private CharacterController _characterController;
        [SerializeField] 
        private LifeController _lifeController;
        [SerializeField] 
        float _enemyPushForce = 3f;
        [Header("Weapons References")]
        [SerializeField]
        private PlayerWeaponHandler _playerWeaponHandler;
        [SerializeField]
        private InputActionReference _shootAction;
        [SerializeField]
        private InputActionReference _setFirstWeaponAction;
        [SerializeField]
        private InputActionReference _setSecondWeaponAction;
        [SerializeField]
        private FirstPersonController _firstPersonController;

        private bool _beingPushed;
        private Vector3  _pushForce;
        private float _timeSincePushed;
        private bool _shooting;

        private void Awake()
        {
            _lifeController.OnDeath += HasDeath;
            _lifeController.OnBeingHit += HasBeenHit;
            
            _shootAction.action.performed += _ => _shooting = true;
            _shootAction.action.canceled += _ => _shooting = false;
            _setFirstWeaponAction.action.performed += _ => ChangeWeapon(0);
            _setSecondWeaponAction.action.performed += _ => ChangeWeapon(1);
        }
        
        private void HasBeenHit(Vector3 attackDirection)
        {
            _pushForce = attackDirection.normalized * _enemyPushForce;
           _timeSincePushed = Time.time;
           _beingPushed = true;
           _playerSounds.PlaySound(_playerSounds._hurtSound);
        }
        
        private void HasDeath()
        {
            _firstPersonController.enabled = false;
            CameraChanger.SetDeathCamera();
        }

        private void FixedUpdate()
        {
            CheckIfShoot();
            CheckIfPush();
        }

        private void CheckIfShoot()
        {
            if (!_shooting) return;
            
            ShootCurrentWeapon();
        }

        private void ShootCurrentWeapon()
        {
            _playerWeaponHandler.TryShootCurrentWeapon();
        }

        private void ChangeWeapon(int index)
        {
            _playerWeaponHandler.ChangeWeapon(index);
        }

        private void CheckIfPush()
        {
            if (!_beingPushed) return;
            
            PushPlayer();
            
            if (Time.time - _timeSincePushed > 0.05f)
                _beingPushed = false;
        }

        private void PushPlayer()
        {
            var velocity = _characterController.velocity;
            velocity += _pushForce;
            _characterController.Move(velocity * Time.deltaTime);
        }
    }
}