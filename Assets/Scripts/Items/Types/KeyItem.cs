using UnityEngine;
using Utilities;

namespace Items.Types
{
    public class KeyItem : ItemBase
    {
        [SerializeField] private string _keyId;
        
        public string KeyId => _keyId;
        
        protected override void ItemPicked()
        {
            base.ItemPicked();
            NotificationController.ShowNotification("llave " + _keyId + "Recogida", NotificationType.Ammo);
        }
    }
}