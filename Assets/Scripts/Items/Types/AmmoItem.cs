using UnityEngine;
using Utilities;
using Weapons;

namespace Items.Types
{
    public class AmmoItem : ItemBase
    {
        [SerializeField] WeaponType _weaponType;
        [SerializeField] int _ammoAmount;
        
        public WeaponType WeaponType => _weaponType;
        public int AmmoAmount => _ammoAmount;

        protected override void ItemPicked()
        {
            base.ItemPicked();
            NotificationController.ShowNotification("Municion de " + _weaponType + " recogida", NotificationType.Ammo);
        }
    }
}