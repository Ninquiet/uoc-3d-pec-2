using UnityEngine;

namespace Sounds
{
    public class ZombieSounds : BaseSoundPlayer
    {
        public AudioClip _AttackSound;
        public AudioClip _hurtSound;
        public AudioClip _idleSound;
    }
}