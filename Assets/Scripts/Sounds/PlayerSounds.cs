using UnityEngine;

namespace Sounds
{
    public class PlayerSounds : BaseSoundPlayer
    {
        public AudioClip _ShotSound;
        public AudioClip _pickObjectSound;
        public AudioClip _hurtSound;
    }
}