using UnityEngine;

namespace Sounds
{
    public class BaseSoundPlayer : MonoBehaviour
    {
        [SerializeField] private AudioSource _audioSource;
        
        public void PlaySound(AudioClip playSound)
        {
            _audioSource.PlayOneShot(playSound);
        }
    }
}